"""

Read the connection status for a given list of devices by their mac address
and saves the information of the online devices into two files:

ONLINE_FILE:
	ONLINE_NAMES=name1,name2
	ONLINE_IP=ip1,ip2
	ONLINE_COUNT=2
WLAN_FILE:
	mac1|ip1|name1
	mac2|ip2|name2
	
Input file needs to contain the mac address and the owner/name of the device
MAC_LIST_FILE:
	mac1|name1
	mac2|name2


@Werner Kuehn  07.01.2016    Use on your own risk

"""

__version__ = '0.0.2'

from fritzconnection import FritzHosts
import argparse

# Defaults:
FRITZ_IP_ADDRESS    = '192.168.178.1'
FRITZ_TCP_PORT      = 49000
FRITZ_USERNAME      = 'dslf-config'
MAC_LIST_FILE       = 'mac.list'
ONLINE_FILE         = 'online.list'
WLAN_FILE           = 'wlan.list'


def get_active_devices(address=FRITZ_IP_ADDRESS,
                       port=FRITZ_TCP_PORT,
                       user=FRITZ_USERNAME,
                       password='',
                       mac_list_file=MAC_LIST_FILE,
                       online_file=ONLINE_FILE,
                       wlan_file=WLAN_FILE):
	if mac_list_file and type(mac_list_file) is list:
		mac_list_file = mac_list_file[0]
	if online_file and type(online_file) is list:
		online_file = online_file[0]
	if wlan_file and type(wlan_file) is list:
		wlan_file = wlan_file[0]
	o_names=''
	o_ip=''
	o_count=0
	delimiter = ''

	maclist = open(mac_list_file, 'r').readlines()
	hosts = FritzHosts(address=address,
                       	   port=port,
                           user=user,
                           password=password)
	wlanlist = []
	for mac in maclist:
		macEntry = mac.split('|')
		host = hosts.get_specific_host_entry(macEntry[0])
		if host.get('NewActive') == '1':
			o_count += 1
			o_names = '{} {}'.format(o_names,macEntry[1].rstrip())
			o_ip    = '{}{}{}'.format(o_ip,delimiter,host.get('NewIPAddress'))
			delimiter = ','
			wlanlist.append('{}|{}|{}'.format(macEntry[0],host.get('NewIPAddress'),macEntry[1]))
	online_out = open(online_file, 'w')
	online_out.write('ONLINE_NAMES={}\nONLINE_IP={}\nONLINE_COUNT={}\n'.format(o_names,o_ip,o_count))
	online_out.close()
	wlan_out = open(wlan_file, 'w')
	wlan_out.write(''.join(wlanlist))
	wlan_out.close()
	return o_count

# ---------------------------------------------------------
# cli-section:
# ---------------------------------------------------------

def get_cli_arguments():
    parser = argparse.ArgumentParser(description='Wer ist zuhause?')
    parser.add_argument('-i', '--ip-address',
                        nargs='?', default=FRITZ_IP_ADDRESS,
                        dest='address',
                        help='Specify ip-address of the FritzBox to connect to. '
                             'Default: %s' % FRITZ_IP_ADDRESS)
    parser.add_argument('--port',
                        nargs='?', default=FRITZ_TCP_PORT,
                        help='Port of the FritzBox to connect to. '
                             'Default: %s' % FRITZ_TCP_PORT)
    parser.add_argument('-u', '--username',
                        nargs=1, default='',
                        help='Fritzbox authentication username')
    parser.add_argument('-p', '--password',
                        nargs=1, default='',
                        help='Fritzbox authentication password')
    parser.add_argument('-P', '--passwordfile',
                        nargs=1, default='',
                        help='Fritzbox authentication password file: Path/Name')
    parser.add_argument('-m', '--maclistfile',
                        nargs=1, default=MAC_LIST_FILE,
                        help='Mac list file to be scanned: Path/Name. '
                             'Default: %s' % MAC_LIST_FILE)
    parser.add_argument('-o', '--onlinefile',
                        nargs=1, default=ONLINE_FILE,
                        help='Online file being written: Path/Name. '
                             'Default: %s' % ONLINE_FILE)
    parser.add_argument('-w', '--wlanfile',
                        nargs=1, default=WLAN_FILE,
                        help='Online WLAN file being written: Path/Name. '
                             'Default: %s' % WLAN_FILE)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
	args = get_cli_arguments()
	if args.passwordfile != '':
		args.password = open(args.passwordfile[0], 'r').readlines()
	if args.password == '':
		print('You will need to specify at least one of: (-P passwordfile) or (-p password)')
		exit(1)
	active = get_active_devices(args.address, 
								args.port, 
								args.username, 
								args.password, 
								args.maclistfile, 
								args.onlinefile, 
								args.wlanfile)
	if active == 1:
		print('{} device is active'.format(active))
	else:
		print('{} devices are active'.format(active))
